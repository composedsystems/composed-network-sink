# Changelog
All noteable changes to this project will be documented in this file. 

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [0.1.1] - 2020-02-21
### Removed
- Stale example code with bad linkages


## [0.1.0] - 2020-01-21
### Added
- Everything (Initial publication)