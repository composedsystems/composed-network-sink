# README
Composed Network Sink is an extension to the Composed Event logger. 

## Getting Started

* Install this package through GPM

* Use the public constructor and add a Network Sink to your log instance as you would any other sink

* Use the Network Sink Endpoint to collect networked log events and write them to a local log instance

### Examples

* An example demonstrating the network sink and endpoint functionality is included in the package

### Dependencies

* This packages depends on @cs/event-logger

## Contribution Guidelines

* All contributions must adhere to SOLID design principals.

* All code must be unit tested to the extent reasonably possible.

* Please contact the author if you want to contribute.

## Who do I talk to?

* Paul Ross | Composed Systems LLC
* paul.ross@composedsystems.com

## License

See the [LICENSE.md](LICENSE.md) file for details